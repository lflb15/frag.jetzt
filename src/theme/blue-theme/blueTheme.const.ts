export const blue = {

  '--primary' : '#80ba24',
  '--primary-variant': '#76aa22',

  '--secondary': '#F4AA00',
  '--secondary-variant': '#d09203',

  '--background': '#22292f',
  '--surface': '#3e4d55', //Oben-Unten und Kasten in der Mitte
  '--dialog': '#43535c', //Dialogfenster --> Impressum etc
  '--cancel': '#354249', //Schließen Button in Dialog
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',

  '--on-primary': '#000000',
  '--on-secondary': '#000000',
  '--on-background': '#ffffff',
  '--on-surface': '#ffffff',  //Dünne Schrift, im Header und Footer und Do you have any questions wahrscheinlich p
  '--on-cancel': '#ffffff',

  '--green': '#4caf50',
  '--red': '#f44336',
  '--yellow': '#FFD54F',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121',
  '--moderator': '#37474f',
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Homework 1',
      'de': 'Hausübung 1'
    },
    'description': {
      'en': 'Darkmode with THM Colors WCAG 2.1 AA',
      'de': 'Darkmode im Design der THM WCAG 2.1 AA'
    }
  },
  'isDark': true,
  'order': -1,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'primary'

};
